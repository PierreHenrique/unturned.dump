﻿namespace Unturned.Dump
{
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class JsonDat
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Values")]
        public List<Values> Values { get; set; }
    }

    public partial class Values
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Value")]
        public string Value { get; set; }
    }

    public partial class JsonDat
    {
        public static List<JsonDat> FromJson(string json) => JsonConvert.DeserializeObject<List<JsonDat>>(json, Settings);
        public static string ToJson(List<JsonDat> self) => JsonConvert.SerializeObject(self, Settings);

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}