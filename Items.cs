﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unturned.Dump
{
    public class Items
    {
        public static List<JsonDat> Bundle = new List<JsonDat>();

        public DirectoryInfo Folder { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public List<Values> Values = new List<Values>();

        private bool TranslationExist { get; set; }
        private bool DatExist { get; set; }

        public Items(DirectoryInfo folder)
        {
            Folder = folder;

            GetTranslation();
            GetDat();

            if (TranslationExist && DatExist)
            {
                Bundle.Add(new JsonDat
                {
                    Name = Name,
                    Description = Description,
                    Values = Values
                });
            }
        }

        private void GetTranslation()
        {
            var p = Path.Combine(Folder.FullName, "English.dat");
            TranslationExist = File.Exists(p);

            if (TranslationExist)
            {
                var lines = File.ReadLines(p);
                foreach (var line in lines.Where(x => !string.IsNullOrEmpty(x)))
                {
                    if (line.Contains(" "))
                    {
                        var prefix = line.Split(' ');
                        var value = line.Substring(line.IndexOf(' ') + 1);

                        switch (prefix[0])
                        {
                            case "Name":
                                Name = value.TrimEnd();
                                break;
                            case "Description":
                                Description = value.TrimEnd();
                                break;
                        }
                    }
                }
            }
        }

        private void GetDat()
        {
            var p = Path.Combine(Folder.FullName, $"{Folder.Name}.dat");
            DatExist = File.Exists(p);

            if (DatExist)
            {
                var lines = File.ReadLines(p);
                foreach (var line in lines.Where(x => !string.IsNullOrEmpty(x)))
                {
                    if (line.Contains(" "))
                    {
                        var prefix = line.Split(' ');
                        var value = line.Substring(line.IndexOf(' ') + 1);

                        Values.Add(new Values
                        {
                            Id = prefix[0],
                            Value = value
                        });
                    }
                    else
                    {
                        Values.Add(new Values
                        {
                            Id = line,
                            Value = "NULL"
                        });
                    }
                }
            }
        }
    }
}