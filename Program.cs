﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Unturned.Dump
{
    class Program
    {
        private static readonly List<Task> Tasks = new List<Task>();

        private static string Root { get; set; }
        private static string Name { get; set; }
        private static string Output { get; set; }

        static void Main(string[] args)
        {
            Root = @"C:\Program Files (x86)\Steam\steamapps\common\Unturned\Bundles\Items\";
            Name = "Dump";
            Output = AppDomain.CurrentDomain.BaseDirectory;

            if (args.Length != 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    var arg = args[i];

                    Console.WriteLine($"{arg}");

                    if (arg.StartsWith("-"))
                    {
                        switch ($"{arg.ToLower()}")
                        {
                            case "-root":
                                Root = $@"{args[i + 1]}";
                                break;
                            case "-name":
                                Name = args[i + 1];
                                break;
                            case "-output":
                                Output = $@"{args[i + 1]}";
                                break;
                        }
                    }
                }
            }

            Run();
        }

        private static void Run()
        {
            var directoryList = Directory.GetDirectories(Root, "*", SearchOption.TopDirectoryOnly).ToList();
            var chunkList = ChunkBy(directoryList, directoryList.Count / 4);

            foreach (var chunk in chunkList.Select(x => x))
            {
                Tasks.Add(Task.Factory.StartNew(() => CreateTask(chunk)));
            }

            Task.WaitAll(Tasks.ToArray());
            Write(Name);
        }

        private static void CreateTask(List<string> rootDirectory)
        {
            foreach (var root in rootDirectory)
            {
                var directory = Directory.GetDirectories(root, "*", SearchOption.AllDirectories);

                foreach (var s in directory)
                {
                    new Items(new DirectoryInfo(s));
                }
            }
        }

        private static void Write(string name)
        {
            using (StreamWriter file = File.CreateText(Path.Combine(Output, $"{name}.json")))
            {
                JsonSerializer serializer = new JsonSerializer {Formatting = Formatting.Indented};
                serializer.Serialize(file, Items.Bundle);

                Console.WriteLine($"File {name} created!\nPath: {Path.Combine(Output, $"{Name}.json")}"); 
            }
        }

        public static List<List<T>> ChunkBy<T>(List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}
